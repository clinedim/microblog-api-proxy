# Microblog API Proxy

NGINX proxy application for our Microblog API.

## Usage

### Environment Variables

* `APP_HOST` - Hostname of the app to which to forward requests (default: `app`).
* `LISTEN_PORT` - The port on which to listen (default: `8000`).
* `APP_PORT` - The port of the app to which to foward requests (default `9000`).
